package com.estragon.chessdiags2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.util.Log;
import android.view.SurfaceHolder;

import com.estragon.chessdiags2.core.Ressources;

import java.text.SimpleDateFormat;
import java.util.Date;

import chesspresso.Chess;
import chesspresso.move.IllegalMoveException;
import chesspresso.position.Position;
import core.Partie;

/**
 * Created by gon on 05/01/15.
 */
public class ChessWatchFaceService extends CanvasWatchFaceService {

    @Override
    public Engine onCreateEngine() {
        /* provide your watch face implementation */
        return new Engine();
    }

    /* implement service callback methods */
    private class Engine extends CanvasWatchFaceService.Engine {

        Paint paint;
        Paint paintHeure;
        Partie partie;
        int highLight = -1;
        int width, height;
        SimpleDateFormat format;


        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);
            paint = new Paint();
            partie = new Partie();
            paintHeure = new Paint();
            paintHeure.setColor(Color.RED);
            paintHeure.setTextSize(50);
            format = new SimpleDateFormat("HH:mm");
        }

        @Override
        public void onPropertiesChanged(Bundle properties) {
            super.onPropertiesChanged(properties);
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();
            short[] coups = partie.getPositionPieces().getAllMoves();
            if (coups != null && coups.length > 0) {
                try {
                    partie.getPositionPieces().doMove(coups[(int) (Math.random()*coups.length)]);
                } catch (IllegalMoveException e) {
                    e.printStackTrace();
                }
            }
            invalidate();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {
            super.onDraw(canvas,bounds);
            try {
                width = bounds.width();
                height = bounds.height();
                float dim = Math.min(bounds.width(), bounds.height());
                float taille = 8;
                float tailleCase = dim / taille;

                canvas.drawBitmap(Ressources.getEchiquier(), null, new RectF(0,0,dim,dim), paint);

                //Dessin des pièces
                Position pos = partie.getPositionPieces();
                int[] conversion = new int[] {0,2,3,4,5,1,6};
                for (int i = 0; i < 64; i++ ) {
                    int piece = conversion[pos.getPiece(Partie.conversionCase(i))];
                    if (piece == 0) continue;
                    int couleur = pos.getColor(Partie.conversionCase(i));
                    dessinCase(canvas,piece + couleur * 6,getPosition(i)[0],getPosition(i)[1],tailleCase,i==highLight);
                }
                canvas.drawText(format.format(new Date()),width/2,height/2,paintHeure);
            }
            catch (Exception e) {
                Log.e("Chessdiags","Erreur",e);
            }
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
        }

        public float[] getPosition(int i) {
            return getPosition(i,false);
        }

        public float[] getPosition(int i,boolean center) {
            int taille = 8;
            float tailleCase = Math.min(width, height) / 8.0f;
            int x = (i%taille);
            int y = ((int) (i/taille));
            if (isInverse()) {
                y = 7 - y;
                x = 7 - x;
            }
            if (center) {
                return new float[] {x * tailleCase + tailleCase / 2,y * tailleCase + tailleCase /2};
            }
            else return new float[] {x * tailleCase,y * tailleCase};
        }



        public void dessinCase(Canvas canvas, int piece, float offsetx, float offsety, float tailleCase, boolean choisie) {
            if (choisie) {
                canvas.save();
                canvas.clipRect(new RectF(offsetx,offsety,offsetx+tailleCase,offsety+tailleCase));
                canvas.drawColor(Color.GREEN);
                canvas.restore();
            }

            if (choisie) paint.setAlpha(150);
            canvas.drawBitmap(Ressources.pieces[piece],null, new RectF(offsetx,offsety,offsetx+tailleCase,offsety+tailleCase), paint);
            paint.setAlpha(255);
        }

        public void dessinCase(Canvas canvas, int piece, float offsetx, float offsety, float tailleCase) {
            dessinCase(canvas,piece,offsetx,offsety,tailleCase,false);
        }

        public boolean isInverse() {
            return partie.getCouleurDefendue() != Chess.WHITE;
        }

    }
}