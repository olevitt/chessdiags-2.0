package com.estragon.chessdiags2;

import android.content.Context;
import android.support.wearable.view.WearableListView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class CustomWearableListAdapter extends WearableListView.Adapter {

    private Context context;

    public CustomWearableListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public WearableListView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(android.R.layout.simple_list_item_1, null);
        MyHolder mh = new MyHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(WearableListView.ViewHolder viewHolder, int i) {
        MyHolder holder = (MyHolder) viewHolder;
        holder.txt1.setText("ok"+i);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public static class MyHolder extends WearableListView.ViewHolder {
        protected TextView txt1;

        private MyHolder(View v) {
            super(v);
            this.txt1 = (TextView) v.findViewById(android.R.id.text1);
        }
    }

}
