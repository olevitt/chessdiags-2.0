package com.estragon.chessdiags2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.io.InputStream;

import chesspresso.position.FEN;
import core.Partie;
import core.PartieListener;
import core.Problem;
import widgets.Board;

public class MainActivity extends Activity implements PartieListener, Board.BoardListener, ChoixPromotionDialog.PromotionCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, DataApi.DataListener, MessageApi.MessageListener,
        NodeApi.NodeListener {

    private Board board;
    private int caseChoisie;
    private Partie partie;
    private Problem problem;
    private int mode = MODE_VSWATCH;
    private GoogleApiClient mGoogleApiClient;
    private boolean connected = false;

    public static int MODE_VSWATCH = 0, MODE_2PLAYERS = 1, MODE_PROBLEM = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey("payload")) {
            String payload = new String(getIntent().getByteArrayExtra("payload"));
            Log.i("Chessdiags","payload : "+payload);
            String[] split = payload.split("\\|");
            if (split.length != 2) {
                problem = new Problem();
                problem.setPosition(FEN.START_POSITION);
                problem.setNbMoves(10000);
            }
            else {
                problem = new Problem();
                problem.setPosition(split[0]);
                problem.setNbMoves(Integer.parseInt(split[1]));
                mode = MODE_PROBLEM;
            }
        }
        else {
            choisirMode();
            problem = new Problem();
            problem.setPosition(FEN.START_POSITION);
            problem.setNbMoves(10000);
        }

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                board = (Board) stub.findViewById(R.id.text);
                newGame();
                board.addListener(MainActivity.this);
                board.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        choisirMode();
                        return false;
                    }
                });
            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        /*Collection<String> nodes = getNodes();
        if (nodes.size() > 0) {
            Wearable.MessageApi.sendMessage(mGoogleApiClient, nodes.iterator().next(), "/newProblem", null);
        }
        else {
            Toast.makeText(this,"Not connected to a device",Toast.LENGTH_LONG).show();
        }*/





        /*WearableListView liste = (WearableListView) findViewById(R.id.pager);
        liste.setAdapter(new CustomWearableListAdapter(this));*/
    }

   /* private Collection<String> getNodes() {
        HashSet<String> results= new HashSet<String>();
        NodeApi.GetConnectedNodesResult nodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : nodes.getNodes()) {
            results.add(node.getId());
        }
        return results;
    }*/

    public void choisirMode() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Mode ?")
                .setItems(R.array.wearModes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        modeChoisi(which);
                    }
                });
        builder.show();
    }

    public void modeChoisi(int which) {
        mode = which;
        if (which == MODE_VSWATCH) {
            problem = new Problem();
            problem.setPosition(FEN.START_POSITION);
            problem.setNbMoves(10000);
            newGame();
        }
        if (which == MODE_2PLAYERS) {
            problem = new Problem();
            problem.setPosition(FEN.START_POSITION);
            problem.setNbMoves(10000);
            newGame();
        }
        if (which == MODE_PROBLEM) {
            if (mGoogleApiClient.isConnected()) {
                Toast.makeText(this,"Ok",Toast.LENGTH_LONG).show();
                PutDataMapRequest dataMap = PutDataMapRequest.create("/newProblem");
                //dataMap.getDataMap().putInt(COUNT_KEY, count++);
                PutDataRequest request = dataMap.asPutDataRequest();
                PendingResult<DataApi.DataItemResult> pendingResult = Wearable.DataApi
                        .putDataItem(mGoogleApiClient, request);
            }
            else {
                Toast.makeText(this,"Not connected to a chessdiags phone",Toast.LENGTH_LONG).show();
            }
        }
    }

    public void newGame() {
        partie = new Partie();
        if (mode == MODE_2PLAYERS) {
            partie.setTwoPlayersGame(true);
        }
        try {
            partie.importerProbleme(problem);
        } catch (Partie.ParsingException e) {
            Log.e("Chessdiags","Parsing error",e);
        }
        partie.addPartieListener(MainActivity.this);
        board.chargerPartie(partie);
    }

    @Override
    public void positionChangee() {
        // TODO Auto-generated method stub
        this.runOnUiThread(new Runnable() {
            public void run() {
                majPosition();
            }
        });
    }

    public void majPosition() {
        board.majBoard();
    }

    public void setCaseChoisie(int caseChoisie) {
        this.caseChoisie = caseChoisie;
        board.highLight(caseChoisie);
    }

    @Override
    public void caseClickee(int numCase) {
        if (caseChoisie == numCase) {
            setCaseChoisie(-1);
        }
        else if (caseChoisie == -1) {
            setCaseChoisie(numCase);
        }
        else {
            this.partie.proposerCoup(caseChoisie,numCase);
            setCaseChoisie(-1);
        }
        board.majBoard();
    }


    @Override
    public void partieTerminee(final int resultat) {
        // TODO Auto-generated method stub
                this.runOnUiThread(new Runnable() {
                    public void run() {
                        if (resultat == PartieListener.WIN) {
                            //DAO.diagrammeResolu(problem);
                            new AlertDialog.Builder(MainActivity.this).setMessage(getString(R.string.congratulations)).setNegativeButton(getString(R.string.ok), null).setPositiveButton(R.string.nextproblem, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                    loadNextProblem(true);
                                }
                            }).show();

                        }
                        else {
                            new AlertDialog.Builder(MainActivity.this).setMessage(R.string.fail).setPositiveButton(R.string.ok, null).setNegativeButton(R.string.tryagain, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                    try {
                                        newGame();
                                    }
                                    catch (Exception e) {
                                        Log.e("Diagramme : ", "", e);
                                    }
                                }
                            }).show();
                        }
                    }
                });
            }

    public void loadNextProblem(boolean sens) {
        /*Problem next = ListeProblemes.getListe().getNextProblem(problem.getSource(),problem.getId(),sens);
        if (next == null) {
            if (sens) Toast.makeText(Diagramme.this, R.string.nextproblemnotfound, Toast.LENGTH_LONG).show();
        }
        else {
            Diagramme.this.problem = next;
            try {
                Diagramme.this.partie.importerProbleme(problem);
                refreshTitle();
            }
            catch (Exception e) {
                Log.e("Diagramme : ","",e);
            }
        }*/
    }

    @Override
    public void promotionDemandee() {
        // TODO Auto-generated method stub
        ChoixPromotionDialog.newInstance(this, this, partie.getCouleurDefendue()).show(getFragmentManager(), "dialog");
    }


    @Override
    public void onChoixFait(int promotion) {
        // TODO Auto-generated method stub
        partie.promotionChoisie(promotion);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Wearable.DataApi.removeListener(mGoogleApiClient, this);
        Wearable.MessageApi.removeListener(mGoogleApiClient, this);
        Wearable.NodeApi.removeListener(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Wearable.DataApi.addListener(mGoogleApiClient, this);
        Wearable.MessageApi.addListener(mGoogleApiClient, this);
        Wearable.NodeApi.addListener(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int cause) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Toast.makeText(this,"zut",Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {

    }

    /**
     * Extracts {@link android.graphics.Bitmap} data from the
     * {@link com.google.android.gms.wearable.Asset}
     */
    private Bitmap loadBitmapFromAsset(GoogleApiClient apiClient, Asset asset) {
        if (asset == null) {
            throw new IllegalArgumentException("Asset must be non-null");
        }

        InputStream assetInputStream = Wearable.DataApi.getFdForAsset(
                apiClient, asset).await().getInputStream();

        if (assetInputStream == null) {
            return null;
        }
        return BitmapFactory.decodeStream(assetInputStream);
    }

    @Override
    public void onMessageReceived(MessageEvent event) {
    }

    @Override
    public void onPeerConnected(Node node) {
    }

    @Override
    public void onPeerDisconnected(Node node) {
    }
}
