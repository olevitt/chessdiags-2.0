package com.estragon.chessdiags2;

import android.content.Context;
import android.support.wearable.view.GridPagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by gon on 12/07/14.
 */
public class CustomAdapter extends GridPagerAdapter {

    private Context context;

    public CustomAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getRowCount() {
        return 2;
    }

    @Override
    public int getColumnCount(int i) {
        return 2;
    }

    @Override
    protected Object instantiateItem(ViewGroup viewGroup, int i, int i2) {
        ImageView view = new ImageView(context);
        view.setImageResource(R.drawable.echiquier);
        viewGroup.addView(view);
        return view;
    }

    @Override
    protected void destroyItem(ViewGroup viewGroup, int i, int i2, Object o) {

    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return false;
    }
}
