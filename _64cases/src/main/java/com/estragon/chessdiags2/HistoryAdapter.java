package com.estragon.chessdiags2;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import core.History;
import donnees.ListeHistory;

public class HistoryAdapter extends ArrayAdapter<History>  {

	public HistoryAdapter(Context context) {
		super(context, android.R.layout.simple_list_item_1);
		charger();
	}
	
	public void charger() {
		Log.i("Chessdiags", "Mise à jour de l'historique");
		setNotifyOnChange(false);
		clear();
		ListeHistory liste = ListeHistory.getListe();
		for (History history : liste) {
			add(history);
		}
	}
	

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		Log.i("ChessDiags", "Adapter history");
		charger();
		super.notifyDataSetChanged();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		//TODO : optimiser avec convertView
		History history = getItem(position);
		View rowView = convertView;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.problemitem, parent, false);
		}
		
		if (history.getProblem() == null) {
			return rowView;
		}
		
		TextView textView = (TextView) rowView.findViewById(R.id.label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
		TextView subtitle = (TextView) rowView.findViewById(R.id.subtitle);
		textView.setText(history.getProblem().getNom());
		if (history.getProblem().isResolu()) imageView.setImageResource(android.R.drawable.star_big_on);
		else imageView.setImageResource(android.R.drawable.star_big_off);
		subtitle.setText("Mate in "+history.getProblem().getNbMoves());
		return rowView;
	}

}
