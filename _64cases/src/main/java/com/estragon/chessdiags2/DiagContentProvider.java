package com.estragon.chessdiags2;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import sql.DatabaseHelper;

public class DiagContentProvider extends ContentProvider {

	DatabaseHelper helper;
	private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	private static final String AUTHORITY = "com.estragon.chessdiags2.provider";

	private static final String PROBLEM = "problem";
	private static final String SOURCE = "source";
	private static final int CODE_PROBLEM = 1, CODE_SOURCE = 2;

	static {
		sURIMatcher.addURI(AUTHORITY, PROBLEM, CODE_PROBLEM);
		sURIMatcher.addURI(AUTHORITY, SOURCE, CODE_SOURCE);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {

		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean onCreate() {
		helper = new DatabaseHelper(getContext());
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// Uisng SQLiteQueryBuilder instead of query() method
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		// Set the table
		int typeUri = sURIMatcher.match(uri);
		switch (typeUri) {
		case CODE_PROBLEM:
			queryBuilder.setTables("problem");
			break;
		case CODE_SOURCE:
			queryBuilder.setTables("source");
			break;
		}



		SQLiteDatabase db = helper.getWritableDatabase();
		Cursor cursor = queryBuilder.query(db, projection, selection,
				selectionArgs, null, null, sortOrder);
		// Make sure that potential listeners are getting notified
		cursor.setNotificationUri(getContext().getContentResolver(), uri);

		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		String table = "problem";
		// Set the table
		int typeUri = sURIMatcher.match(uri);
		switch (typeUri) {
		case CODE_PROBLEM:
			queryBuilder.setTables("problem");
			break;
		}
		SQLiteDatabase db = helper.getWritableDatabase();
		int updateCount = db.update(table, values, selection, selectionArgs);
		Log.i("Chessdiags","Update : "+updateCount);
		if (updateCount > 0) {
			getContext().getContentResolver().notifyChange(uri, null); 
		}

		return updateCount;
	}

}
