package donnees;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import core.History;
import sql.DAO;

public class ListeHistory extends ArrayList<History> implements Comparator<History> {

	public static ListeHistory getListe() {
		ListeHistory liste = new ListeHistory();
		try {
			liste.addAll(DAO.getHistoryDao().queryForAll());
		}
		catch (Exception e) {
			Log.e("Chessdiags","",e);
		}
		Collections.sort(liste,Collections.reverseOrder(liste));
		return liste;
	}

	@Override
	public int compare(History lhs, History rhs) {
		// TODO Auto-generated method stub
		return lhs.getDate().compareTo(rhs.getDate());
	}

}
