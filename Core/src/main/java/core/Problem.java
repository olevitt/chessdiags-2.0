package core;

import com.j256.ormlite.field.DatabaseField;

import org.json.JSONException;
import org.json.JSONObject;

import donnees.ListeProblemes;
import sql.DAO;

public class Problem {

    public static int MODE_NORMAL = 0, MODE_SURVIVOR = 1;
	
	@DatabaseField
	String position;
	@DatabaseField
	String nom = "";

	@DatabaseField
	String description = "";
	@DatabaseField
	int id = -1;
	@DatabaseField
	int source = 1;
	@DatabaseField
	int nbMoves = 2;
	@DatabaseField
	boolean resolu;
	@DatabaseField(allowGeneratedIdInsert=true,index = true,generatedId=true) 
	int internalId;
    @DatabaseField
    int tempsAnalyse = 500;
    @DatabaseField
    int gameMode = MODE_NORMAL;
	
	boolean sauvegarde = false;

	public Problem(int id,int source,String position,int difficulty,boolean resolu) {
		this(id,source,position,difficulty,resolu,"","",2);
	}
	
	public Problem() {
		
	}
	
	public Problem(int id,int source,String position,int difficulty,boolean resolu,String nom,String description, int nbMoves) {
		this.id = id;
		this.source = source;
		this.position = position;
		this.resolu = resolu;
		setNom(nom);
		setDescription(description);
		setNbMoves(nbMoves);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
		if (description == null) description = "";
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public int getNbMoves() {
		return nbMoves;
	}

	public void setNbMoves(int nbMoves) {
		this.nbMoves = nbMoves;
	}
	

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public boolean isResolu() {
		return resolu;
	}

	public void setResolu(boolean resolu) {
		this.resolu = resolu;
	}
	
	public Problem(JSONObject json) throws JSONException {
		this.id = json.getInt("id");
		this.position = json.getString("position");
		setDescription(json.getString("description"));
		setNom(json.getString("name"));
		setNbMoves(json.getInt("moves"));
        setTempsAnalyse(json.optInt("analysisTime", 0));
        setGameMode(convertGameMode(json.optString("gameMode","")));
	}

    private int convertGameMode(String gameMode) {
        if ("survivor".equalsIgnoreCase(gameMode)) {
            return MODE_SURVIVOR;
        }
        else {
            return MODE_NORMAL;
        }
    }

    private String convertGameMode(int gameMode) {
        if (gameMode == MODE_SURVIVOR) {
            return "survivor";
        }
        else {
            return null;
        }
    }
	
	public JSONObject getJSON() {
		JSONObject json = new JSONObject();
		try {
			json.put("position", position);
			json.put("description", description);
			json.put("name", getNom(true));
			json.put("moves", getNbMoves());
            if (getTempsAnalyse() != 0) {
                json.put("analysisTime", getTempsAnalyse());
            }
            if (convertGameMode(gameMode) != null) {
                json.put("gameMode",convertGameMode(gameMode));
            }
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public void setSauvegarde(boolean sauvegarde) {
		this.sauvegarde = sauvegarde;
	}

	public void sauvegarder() {
		if (ListeProblemes.getListe().getProblem(id, source) == null) {
			ListeProblemes.getListe().add(this);
		}
		try {
			sauvegarde = true;
			DAO.getProblemDao().createOrUpdate(this);
		}
		catch (Exception e) {
			
		}
	}
	
	public boolean isEditable() {
		return getSource() == 1;
	}
	
	public boolean isDeletable() {
		return getSource() == 1;
	}
	
	public boolean isSauvegarde() {
		return sauvegarde;
	}
	
	public String getNom() {
		return getNom(false);
	}
	
	public String getNom(boolean real) {
		if ((nom == null || nom.trim().equals("")) && !real) return "N/A";
		else if (real && nom == null) return "";
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
		if (nom == null) nom = "";
	}
	
	public int getInternalId() {
		return internalId;
	}
	
	public void setInternalId(int internalId) {
		this.internalId = internalId;
	}

    public int getTempsAnalyse() {
        return tempsAnalyse;
    }

    public void setTempsAnalyse(int tempsAnalyse) {
        this.tempsAnalyse = tempsAnalyse;
    }

    public int getGameMode() {
        return gameMode;
    }

    public void setGameMode(int gameMode) {
        this.gameMode = gameMode;
    }
}
