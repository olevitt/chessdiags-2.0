package com.estragon.chessdiags2.core;

/**
 * Created by gon on 11/07/14.
 */
public class ProblemDTO {

    public String fen;

    public String getFen() {
        return fen;
    }

    public void setFen(String fen) {
        this.fen = fen;
    }
}
