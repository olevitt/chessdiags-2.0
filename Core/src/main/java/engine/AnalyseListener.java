package engine;

import core.Coup;

public interface AnalyseListener {
	public void bestMoveFound(Coup c);
}
